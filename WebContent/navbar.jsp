
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand"><!--Sharda University --> IIIT KOTA</a>
    </div>
    <div>
      <ul class="nav navbar-nav">
        <li><a href="dashboard.jsp"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li><a href="addSubjects.jsp"><span class="glyphicon glyphicon-book"></span> Add Subjects</a></li>
        <li><a href="addFaculty.jsp"><span class="glyphicon glyphicon-user"></span> Add Faculty</a></li> 
        <li><a href="addCourse.jsp"><span class="glyphicon glyphicon-education"></span> Add Course</a></li> 
		<li><a href="addClassroom.jsp"><span class="glyphicon glyphicon-education"></span> Add Classroom</a></li> 
		<li><a href="edit.timing.php"><span class="glyphicon glyphicon-time"></span> Timing</a></li> 
		<li><a href="editTiming.jsp"><span class="glyphicon glyphicon-time"></span> Timing</a></li> 
		<!--<li><a href="teaching.load.php">Teaching Load</a></li> 
		<li><a href="subject.allocation.php">Subject Allocation</a></li> 
		<li><a href="help.php">Help</a></li>  -->
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-user"></span>Welcome <?php if(@$_SESSION['name'])echo $_SESSION['name']; ?></a></li>
        <li><a href="logout.jsp"><span class="glyphicon glyphicon-log-in"></span> LogOut</a></li>
      </ul>
    </div>
  </div>
</nav>




</body>
</html>